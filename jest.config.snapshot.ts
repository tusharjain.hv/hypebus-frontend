import type {Config} from 'jest';

const config: Config = {
	preset: "ts-jest",
	testEnvironment: "jsdom",
	forceExit: true,
    verbose: true,
    testMatch:[ "**/*.snapshot.test.ts?(x)" ],
	transform: {
		"^.+\\.(ts|tsx)$": "ts-jest",
	},
	transformIgnorePatterns: ["/node_modules/(?!(axios)/)"],
    moduleNameMapper: {
        "\\.(jpg|jpeg|png|svg)$": "<rootDir>/__mocks__/styleMock.ts",
		"\\.(css|less|sass|scss)$": "<rootDir>/__mocks__/styleMock.ts",
	},
};

export default config;
