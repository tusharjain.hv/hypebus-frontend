import type {Config} from 'jest';

const config: Config = {
	preset: "ts-jest",
	testEnvironment: "jsdom",
	forceExit: true,
    verbose: true,
    testMatch:[ "**/*.unit.test.ts?(x)" ],
	transform: {
		"^.+\\.(ts|tsx)$": "ts-jest",
	},
	transformIgnorePatterns: ["/node_modules/(?!(axios)/)"],
	moduleNameMapper: {
		"\\.(css|less|sass|scss)$": "<rootDir>/__mocks__/styleMock.ts",
	},
};

export default config;
