import type {Config} from 'jest';

const config: Config = {
	preset: "jest-puppeteer",
	testMatch: ["**/*.e2e.test.ts?(x)"],
	// testEnvironment: "jsdom",
	 transform: {
		"^.+\\.(ts|tsx)$": "ts-jest",
	},
	globals: {
		URL: process.env.REACT_APP_HYPERBUS_URL || "http://localhost:3000",
	},
	verbose: true,
};
export default config;