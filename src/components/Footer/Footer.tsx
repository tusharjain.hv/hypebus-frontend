import "./Footer.css";
import Heart from "../../assets/images/icons/Heart.svg";

function Footer() {
	return (
		<div className='footer' id='bottom'>
			<p>Made by Tushar Jain</p>
		</div>
	);
}
export default Footer;
