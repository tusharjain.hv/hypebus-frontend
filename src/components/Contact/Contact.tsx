import "./Contact.css";

function Contact() {
	return (
		<div className='container pb-5' id='contact'>
			<div className='row'>
				<div className='col-12 text-center contactSection'>
					<h4 className='mainHeading' data-testid='main-heading'>
						Contact Info
					</h4>
					<h4 className='smallText px-md-5 mx-md-5'>
						You can reach us via E-Mail or Call or via any other platforms link
						provided.
					</h4>
					<a
						href='mailto:tusharjain0022@gmail.com'
						className='link'
						data-testid='email'
						target='_blank'
						rel='noreferrer'>
						{" "}
						<p className='readButton mt-4 mx-md-2'>E-mail</p>
					</a>
					<a
						href='https://www.linkedin.com'
						className='link'
						target='_blank'
						data-testid='phone'
						rel='noreferrer'>
						{" "}
						<p className='readButton mx-md-2'>+91 987892636177</p>
					</a>
				</div>
			</div>
		</div>
	);
}
export default Contact;
