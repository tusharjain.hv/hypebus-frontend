import { Drawer } from "@mui/material";
import SquareIcon from "@mui/icons-material/Square";
import SeatMap from "../SeatMap/SeatMap";
import { busesSelector } from "../../features/buses/BusSlice";
import { useSelector } from "react-redux";

function SelectSeatDrawer(props: any) {
	const { selectedSeats, selectedBus } = useSelector(busesSelector);
	return (
		<Drawer
			anchor='right'
			open={props.drawerIsOpen}
			onClose={props.toggleDrawer(false)}
			style={{ width: "40%", textAlign: "center" }}>
			<h3 style={{ marginTop: "30px" }} data-testid='seat-map-heading'>
				Select Your Seat
			</h3>
			<div className='d-flex justify-content-between mx-4 my-3'>
				<p>
					<SquareIcon style={{ color: "black" }} />
					Occupied
				</p>
				<p>
					<SquareIcon style={{ color: "grey" }} />
					Not Occupied
				</p>
				<p>
					<SquareIcon style={{ color: "#f0394b" }} />
					Selected
				</p>
			</div>
			<div className='seat-map container' style={{ margin: "auto", padding: "0px 100px" }}>
				<div style={{ width: "280px", background: "lightgray" }}>
					<p>Front</p>
				</div>
				<SeatMap {...props} />
			</div>
			<hr style={{ margin: "30px 20px 10px 20px", border: "1px solid black" }}></hr>
			<div
				style={{ width: "100%", padding: "0px 50px", fontSize: "18px" }}
				className='d-flex justify-content-between'>
				<b>Seat No.</b>
				<p>
					{selectedSeats &&
						selectedSeats.map(
							(seat: number, index: number) =>
								`${seat}${index + 1 === selectedSeats.length ? "" : ", "}`
						)}
				</p>
			</div>
			<div
				style={{ width: "100%", padding: "0px 50px", fontSize: "18px" }}
				className='d-flex justify-content-between'>
				<b>Amount</b>
				<p>
					<span style={{ marginRight: "10px" }}>&#8377;</span>
					<b>{selectedBus.fare * selectedSeats.length}</b>
				</p>
			</div>
			<hr style={{ margin: "0px 20px 10px 20px", border: "1px solid black" }}></hr>
			<button
				style={{ margin: "10px 20px 40px auto" }}
				className='red-button'
				onClick={() => {
					props.setPassengerDrawerIsOpen(true);
					props.setDrawerIsOpen(false);
				}}>
				Proceed
			</button>
		</Drawer>
	);
}
export default SelectSeatDrawer;
