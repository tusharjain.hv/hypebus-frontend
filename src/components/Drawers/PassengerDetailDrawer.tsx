import { Drawer, TextField } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { errorHandler } from "../../utils/errorHandler";
import { makeBooking } from "../../api/makeBooking";
import { BookingType } from "../../utils/types";
import { useNavigate } from "react-router-dom";
import Notify from "../Notify";
import { busesSelector } from "../../features/buses/BusSlice";
import { useSelector, useDispatch } from "react-redux";
import { bookingsSelector, setNewBooking } from "../../features/bookings/BookingSlice";
import { userSelector } from "../../features/users/UserSlice";
import PassengerDetailForm from "../Forms/PassengerDetailForm";

function PassengerDetailDrawer(props: any) {
	const dispatch = useDispatch();
	let navigate = useNavigate();
	const { selectedSeats, selectedBus } = useSelector(busesSelector);
	const { newBooking } = useSelector(bookingsSelector);
	const { token } = useSelector(userSelector);

	const handleSubmit = async () => {
		try {
			let booking: BookingType = {
				...newBooking,
				busId: selectedBus._id,
				seats: selectedSeats,
			};
			await makeBooking(token, booking);
			Notify("Booking is Successful", "success");
			navigate("/mybookings");
			props.setPassengerDrawerIsOpen(false);
		} catch (error) {
			console.log(error);
			errorHandler(error);
		}
	};
	return (
		<Drawer
			anchor='right'
			open={props.passengerDrawerIsOpen}
			onClose={props.togglePassengerDrawer(false)}
			style={{ width: "40%", textAlign: "center" }}>
			<ArrowBackIcon
				style={{
					position: "absolute",
					top: "35px",
					left: "20px",
					fontSize: "31px",
				}}
				onClick={() => {
					props.setPassengerDrawerIsOpen(false);
					props.setDrawerIsOpen(true);
				}}
				role='button'></ArrowBackIcon>
			<h3 style={{ marginTop: "30px" }}>Passenger Details</h3>
			{selectedSeats.map((seat: number, id: number) => {
				return <PassengerDetailForm seat={seat} id={id} />;
			})}
			<TextField
				style={{ margin: "3px 20px 20px 20px" }}
				id='outlined-basic'
				name='phone'
				label='Phone No.'
				value={newBooking.phone}
				onChange={(event) => {
					dispatch(setNewBooking({ ...newBooking, phone: event.target.value }));
				}}
				variant='outlined'
			/>

			<hr style={{ margin: "0px 20px 10px 20px", border: "1px solid black" }}></hr>
			<button
				style={{ margin: "10px 20px 40px auto", width: "220px" }}
				className='red-button'
				onClick={handleSubmit}>
				Complete Booking
			</button>
		</Drawer>
	);
}
export default PassengerDetailDrawer;
