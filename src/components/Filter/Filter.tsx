import { Autocomplete, TextField } from "@mui/material";
import { LocalizationProvider, DatePicker } from "@mui/x-date-pickers";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import dayjs from "dayjs";

import "./Filter.css";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { searchBuses } from "../../api/searchBuses";
import { errorHandler } from "../../utils/errorHandler";
import _ from "lodash";
import { setBuses } from "../../features/buses/BusSlice";
import { useDispatch } from "react-redux";

function Filter(props: any) {
	const dispatch = useDispatch();
	const UpdateList = async () => {
		try {
			const busList = await searchBuses({
				...props.filter,
				departure: dayjs(props.filter.departure).toISOString(),
			});
			dispatch(setBuses((busList.data as any).buses));
		} catch (error) {
			console.log(error);
			errorHandler(error);
		}
	};

	return (
		<div className='bus-filter d-flex justify-content-center' style={{ marginTop: "150px" }}>
			<Autocomplete
				disablePortal
				className='combo-box-demo'
				options={props.options}
				onChange={(event: any, newValue: string | null) => {
					let newFilter = _.cloneDeep(props.filter);
					newFilter.from = newValue || "";
					props.setFilter(newFilter);
				}}
				value={props.filter.from}
				sx={{ width: 300 }}
				renderInput={(params) => <TextField {...params} label='From' />}
			/>
			<Autocomplete
				disablePortal
				className='combo-box-demo'
				options={props.options}
				onChange={(event: any, newValue: string | null) => {
					let newFilter = _.cloneDeep(props.filter);
					newFilter.to = newValue || "";
					props.setFilter(newFilter);
				}}
				value={props.filter.to}
				sx={{ width: 300 }}
				renderInput={(params) => <TextField {...params} label='To' />}
			/>
			<LocalizationProvider dateAdapter={AdapterDayjs}>
				<DatePicker
					label='Departure'
					value={props.filter.departure}
					onChange={(newValue) => {
						let newFilter = _.cloneDeep(props.filter);
						newFilter.departure = newValue || null;
						props.setFilter(newFilter);
					}}
					renderInput={(params) => <TextField {...params} />}
				/>
			</LocalizationProvider>
			<button
				className='red-button'
				onClick={UpdateList}
				style={{
					width: "120px",
					marginLeft: "10px",
					borderRadius: "0px",
					padding: "10px",
				}}>
				<SearchOutlinedIcon /> Search
			</button>
		</div>
	);
}
export default Filter;
