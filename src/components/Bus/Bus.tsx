import dayjs from "dayjs";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import Notify from "../Notify";
import { setSelectedBus } from "../../features/buses/BusSlice";
import { useDispatch } from "react-redux";
import { userSelector } from "../../features/users/UserSlice";
import { useSelector } from "react-redux";

function Bus(props: any) {
	const dispatch = useDispatch();
	const { token } = useSelector(userSelector);

	const departure = dayjs(props.bus.departure);
	const arrival = dayjs(props.bus.arrival);

	let minutes = arrival.diff(departure, "minute");
	const hours = Math.floor(minutes / 60);
	minutes = minutes - hours * 60;

	return (
		<div className='bus row' key={props.id}>
			<div className='col-lg-4' style={{ textAlign: "left" }}>
				<h4 style={{ fontSize: "32px" }} data-testid={`bus-name-${props.id}`}>
					{props.bus.name}
				</h4>
				<p>Bus ID - {props.bus._id}</p>
			</div>
			<div className='bus-details col-lg-6 mt-4'>
				<div className='d-flex'>
					<div>
						<p className='red-box'>{props.bus.from}</p>
						<b>{departure.format("DD MMM, HH:mmA")}</b>
					</div>
					<div>
						<ArrowForwardIcon style={{ margin: "5px 20px 22px 20px" }} />
						<p>{`(${hours}hr ${minutes}min)`}</p>
					</div>
					<div>
						<p className='red-box'>{props.bus.to}</p>
						<b>{arrival.format("DD MMM, HH:mmA")}</b>
					</div>
				</div>
			</div>
			<div className='col-lg-2'>
				<h4
					className='ms-auto'
					style={{
						width: "100%",
						fontSize: "32px",
						marginBottom: "10px",
					}}>
					<span style={{ fontSize: "24px", marginRight: "10px" }}>&#8377;</span>
					{props.bus.fare}
				</h4>
				<div className='book-button ms-auto'>
					<p style={{ textAlign: "center" }}>
						{" "}
						{props.bus.seats - props.bus.bookedSeats.length} seats Left
					</p>
					<button
						className='red-button'
						data-testid={`go-button-${props.id}`}
						onClick={() => {
							if (token) {
								dispatch(setSelectedBus(props.bus));
								props.setDrawerIsOpen(true);
							} else {
								Notify("Please login to proceed.", "info");
							}
						}}>
						{" "}
						Go{" "}
					</button>
				</div>
			</div>
		</div>
	);
}
export default Bus;
