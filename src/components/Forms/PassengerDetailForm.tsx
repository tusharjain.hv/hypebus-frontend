import {
	TextField,
	FormControl,
	FormLabel,
	RadioGroup,
	FormControlLabel,
	Radio,
} from "@mui/material";
import _ from "lodash";
import { bookingsSelector, setNewBooking } from "../../features/bookings/BookingSlice";
import { useSelector, useDispatch } from "react-redux";

function PassengerDetailForm(props: any) {
	const dispatch = useDispatch();
	const { newBooking } = useSelector(bookingsSelector);

	const handleChange = (e: { target: { name: any; value: any } }, id: number) => {
		const { name, value } = e.target;
		let passengers: any = _.cloneDeep(newBooking.passengers);
		passengers[id][name] = value;
		dispatch(setNewBooking({ ...newBooking, passengers }));
	};

	return (
		<div
			key={props.id}
			className='passenger-form'
			style={{
				padding: "20px",
				margin: "20px",
				border: "1px solid red",
				borderRadius: "10px",
				textAlign: "left",
			}}>
			<div className='d-flex justify-content-between'>
				<p>Passenger {props.id + 1}</p>
				<p>Seat No. {props.seat}</p>
			</div>

			<TextField
				id='outlined-basic'
				name='name'
				label='name'
				variant='outlined'
				value={newBooking.passengers[props.id].name}
				onChange={(event) => handleChange(event, props.id)}
				style={{ width: "100%", margin: "0px 0px 30px 0px" }}
			/>
			<div className='d-flex'>
				<FormControl>
					<FormLabel
						id='demo-row-radio-buttons-group-label'
						style={{ margin: "0px 40px 0px 0px" }}>
						Gender
					</FormLabel>
					<RadioGroup
						row
						aria-labelledby='demo-row-radio-buttons-group-label'
						name='gender'
						value={newBooking.passengers[props.id].gender}
						onChange={(event) => handleChange(event, props.id)}>
						<FormControlLabel value='female' control={<Radio />} label='Female' />
						<FormControlLabel value='male' control={<Radio />} label='Male' />
					</RadioGroup>
				</FormControl>
				<TextField
					style={{ marginTop: "3px" }}
					type='number'
					id='outlined-basic'
					name='age'
					label='Age'
					value={newBooking.passengers[props.id].age}
					onChange={(event) => handleChange(event, props.id)}
					variant='outlined'
				/>
			</div>
		</div>
	);
}

export default PassengerDetailForm;
