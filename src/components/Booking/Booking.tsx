import dayjs from "dayjs";
import * as React from "react";
import Box from "@mui/material/Box";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import { Passenger } from "../../utils/types";
import { styled } from "@mui/material/styles";
import { tableCellClasses } from "@mui/material/TableCell";
import { bookingsSelector } from "../../features/bookings/BookingSlice";
import { useSelector } from "react-redux";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
	[`&.${tableCellClasses.head}`]: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
	},
	[`&.${tableCellClasses.body}`]: {
		fontSize: 14,
	},
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
	"&:nth-of-type(odd)": {
		backgroundColor: theme.palette.action.hover,
	},
	// hide last border
	"&:last-child td, &:last-child th": {
		border: 0,
	},
}));

function createData(
	busId: string,
	name: string,
	from: string,
	departure: string,
	to: string,
	arrival: string,
	passengerCount: number,
	seats: number[],
	amount: number,
	passengers: Passenger[]
) {
	return {
		busId,
		name,
		from,
		departure,
		to,
		arrival,
		passengerCount,
		seats,
		amount,
		passengers,
	};
}

function Row(props: { row: ReturnType<typeof createData>; id: number }) {
	const { row, id } = props;
	const [open, setOpen] = React.useState(false);

	return (
		<React.Fragment>
			<StyledTableRow sx={{ "& > *": { borderBottom: "unset" } }}>
				<StyledTableCell align='center'>
					<IconButton
						data-testid={`collapse-button-${id}`}
						aria-label='expand row'
						size='small'
						onClick={() => setOpen(!open)}>
						{open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
					</IconButton>
				</StyledTableCell>
				<StyledTableCell
					data-testid={`bus-id-${id}`}
					align='center'
					component='th'
					scope='row'>
					{row.busId}
				</StyledTableCell>
				<StyledTableCell align='center'>{row.name}</StyledTableCell>
				<StyledTableCell align='center'>{row.from}</StyledTableCell>
				<StyledTableCell align='center'>{row.departure}</StyledTableCell>
				<StyledTableCell align='center'>{row.to}</StyledTableCell>
				<StyledTableCell align='center'>{row.arrival}</StyledTableCell>
				<StyledTableCell align='center'>{row.passengerCount}</StyledTableCell>
				<StyledTableCell align='center'>{row.seats.join(",")}</StyledTableCell>
				<StyledTableCell align='center'>{row.amount}</StyledTableCell>
			</StyledTableRow>
			<TableRow>
				<StyledTableCell
					align='center'
					style={{ paddingBottom: 0, paddingTop: 0 }}
					colSpan={10}>
					<Collapse
						data-testid='collapse-menu'
						in={open}
						timeout='auto'
						unmountOnExit
						style={{ margin: "10px" }}>
						<Box sx={{ margin: 1 }}>
							<Typography
								variant='h6'
								gutterBottom
								component='div'
								style={{ marginBottom: "1px solid grey" }}>
								Passenger Details
							</Typography>
							<Table size='small' aria-label='purchases'>
								<TableHead>
									<TableRow>
										<StyledTableCell align='center'>Name</StyledTableCell>
										<StyledTableCell align='center'>Gender</StyledTableCell>
										<StyledTableCell align='center'>Age</StyledTableCell>
										<StyledTableCell align='center'>Seat No.</StyledTableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{row.passengers.map((passengerRow, id) => (
										<TableRow key={passengerRow.name}>
											<StyledTableCell
												align='center'
												component='th'
												scope='row'
												data-testid={`collapse-menu-${id}`}>
												{passengerRow.name}
											</StyledTableCell>
											<StyledTableCell align='center'>
												{passengerRow.gender}
											</StyledTableCell>
											<StyledTableCell align='center'>
												{passengerRow.age}
											</StyledTableCell>
											<StyledTableCell align='center'>
												{row.seats[id]}
											</StyledTableCell>
										</TableRow>
									))}
								</TableBody>
							</Table>
						</Box>
					</Collapse>
				</StyledTableCell>
			</TableRow>
		</React.Fragment>
	);
}

function CollapsibleTable(props: any) {
	const rows: any = [];
	props.bookings.forEach((booking: any) => {
		rows.push(
			createData(
				booking._id,
				booking.name,
				booking.from,
				dayjs(booking.departure).format("DD MMM YY, HH:mmA"),
				booking.to,
				dayjs(booking.arrival).format("DD MMM YY, HH:mmA"),
				booking.passengers.length,
				booking.seats,
				booking.amount,
				booking.passengers
			)
		);
	});

	return (
		<TableContainer component={Paper} style={{ padding: "0px" }}>
			<Table aria-label='collapsible table'>
				<TableHead>
					<TableRow>
						<StyledTableCell />
						<StyledTableCell align='center'>Booking Id</StyledTableCell>
						<StyledTableCell align='center'>Bus Name</StyledTableCell>
						<StyledTableCell align='center'>From</StyledTableCell>
						<StyledTableCell align='center'>Departure</StyledTableCell>
						<StyledTableCell align='center'>To</StyledTableCell>
						<StyledTableCell align='center'>Arrival</StyledTableCell>
						<StyledTableCell align='center'>No. of Passengers</StyledTableCell>
						<StyledTableCell align='center'>Seats</StyledTableCell>
						<StyledTableCell align='center'>Total Amount</StyledTableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{rows.map((row: any, id: number) => (
						<Row key={row.name} row={row} id={id} />
					))}
				</TableBody>
			</Table>
		</TableContainer>
	);
}

function Booking(props: any) {
	const { bookings } = useSelector(bookingsSelector);
	return (
		<div
			className='booking row'
			style={{ margin: "40px 60px", boxShadow: " 5px 3px 10px 1px rgba(0, 0, 0, 0.25)" }}>
			<CollapsibleTable bookings={bookings} />
		</div>
	);
}
export default Booking;
