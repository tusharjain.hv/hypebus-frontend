import EventSeatIcon from "@mui/icons-material/EventSeat";
import _ from "lodash";
import { busesSelector, setSelectedSeats } from "../../features/buses/BusSlice";
import { useSelector, useDispatch } from "react-redux";
import { bookingsSelector, setNewBooking } from "../../features/bookings/BookingSlice";

function Seat(props: any) {
	const dispatch = useDispatch();
	const { selectedSeats, selectedBus } = useSelector(busesSelector);
	const { newBooking } = useSelector(bookingsSelector);

	const removeEmptyPassenger = () => {
		let passengers = _.cloneDeep(newBooking.passengers);
		passengers.pop();
		dispatch(setNewBooking({ ...newBooking, passengers }));
	};
	const addEmptyPassenger = () => {
		let passengers = _.cloneDeep(newBooking.passengers);
		passengers.push({ name: "", gender: "", age: 0 });
		dispatch(setNewBooking({ ...newBooking, passengers }));
	};
	const addSeat = () => {
		let newSelectedSeats = _.cloneDeep(selectedSeats);
		newSelectedSeats.push(props.seatNo);
		dispatch(setSelectedSeats(newSelectedSeats));
	};
	const popSeat = () => {
		let newSelectedSeats = _.cloneDeep(selectedSeats);
		newSelectedSeats = newSelectedSeats.filter((seat: number) => seat !== props.seatNo);
		dispatch(setSelectedSeats(newSelectedSeats));
	};
	const handleSeatClick = () => {
		if (
			!(selectedBus as any).bookedSeats.includes(props.seatNo) && //seat is already booked
			!selectedSeats.includes(props.seatNo) // seat is already selected
		) {
			addEmptyPassenger();
			addSeat();
		} else if (selectedSeats.includes(props.seatNo)) {
			//if seat is already selected, remove it
			removeEmptyPassenger();
			popSeat();
		}
	};

	return (
		<EventSeatIcon
			key={`seat-${props.seatNo}`}
			role='button'
			style={{
				fontSize: "34px",
				color: (selectedBus as any).bookedSeats.includes(props.seatNo)
					? "black"
					: selectedSeats.includes(props.seatNo)
					? "#f0394b"
					: "grey",
			}}
			onClick={handleSeatClick}
		/>
	);
}

function SeatMap(props: any) {
	const seatmap: any = [];
	for (let i = 0; i < 10; i++) {
		let row: any = [];
		for (let j = 0; j < 5; j++) {
			let seatNo = i * 5 + j + 1;

			row.push(
				<div className='col-2' key={seatNo}>
					<Seat {...props} seatNo={seatNo} />
				</div>
			);

			if (j === 1) row.push(<div className='col-2'></div>);
		}
		seatmap.push(
			<div key={i} className='row'>
				{row}
			</div>
		);
		row = [];
	}
	return seatmap;
}

export default SeatMap;
