import "./Intro.css";
import Bus from "../../assets/images/landingBusPic.png";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import { Link } from "react-router-dom";

function Intro() {
	return (
		<div className='landingPage'>
			<div className='container'>
				<div className='row'>
					<div className='col-12 col-lg-7 textBox'>
						<p className='smallText'>#Top 10 Bus Service in India</p>
						<h1>
							HypeBus <br></br>
						</h1>
						<h4>Book your favourite seats now and get 20% Discount</h4>
						<h2 className='smallText'>
							Last Minute Booking - In a hurry to book a bus at the last minute?
							Choose the bus passing from your nearest boarding point and book in a
							few easy steps.
						</h2>
						<div className='d-flex mt-5'>
							{/* <h3>From </h3>

							<h3 style={{ marginRight: "10px", marginLeft: "10px" }}>To</h3>
							<h3>Travel Date</h3> */}
							<Link to='/buses' style={{ textDecoration: "none", color: "white" }}>
								<h3
									style={{
										width: "220px",
										marginLeft: "10px",
										padding: "10px",
										color: "white",
										textAlign: "center",
										textDecoration: "none",
									}}>
									<SearchOutlinedIcon /> Search Buses
								</h3>
							</Link>
						</div>
					</div>
					<div className='col-lg-5 d-none d-lg-block'>
						<img src={Bus} alt='Bus' className='Bus'></img>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Intro;
