import "./Navbar.css";
import { Link } from "react-router-dom";
import { errorHandler } from "../../utils/errorHandler";
import { logoutUser } from "../../api/logout";
import Notify from "../Notify";
import { useEffect } from "react";
import { getUser } from "../../api/getUser";
import { useNavigate } from "react-router-dom";
import { userSelector, setToken, setEmail } from "../../features/users/UserSlice";
import { useSelector, useDispatch } from "react-redux";

function Navbar() {
	const dispatch = useDispatch();
	const { email, token } = useSelector(userSelector);
	let navigate = useNavigate();

	useEffect(() => {
		const userDetail = async () => {
			if (token) {
				const result = await getUser(token);
				dispatch(setEmail(result.email));
			}
		};
		userDetail();
	}, [token]);
	const handleLogout = async () => {
		try {
			await logoutUser(token);
			dispatch(setToken(null));
			Notify("You're logged out successfully!", "info");
			navigate("/buses");
		} catch (error) {
			errorHandler(error);
		}
	};
	return (
		<nav className='navbar fixed-top navbar-expand-lg navbar-light bg-light'>
			<div className='container-fluid'>
				<a data-testid='nav-brand' className='navbar-brand' href='/'>
					HypeBus
				</a>
				<button
					className='navbar-toggler'
					type='button'
					data-bs-toggle='collapse'
					data-bs-target='#navbarNav'
					aria-controls='navbarNav'
					aria-expanded='false'
					aria-label='Toggle navigation'>
					<span className='navbar-toggler-icon'></span>
				</button>
				<div className='collapse navbar-collapse' id='navbarNav'>
					<ul className='navbar-nav ms-auto'>
						<li className='nav-item'>
							<Link data-testid='nav-buses' className='nav-link' to='/buses'>
								Buses
							</Link>
						</li>
						<li className='nav-item'>
							<Link data-testid='nav-about' className='nav-link' to='/#about'>
								About
							</Link>
						</li>
						<li className='nav-item'>
							<Link className='nav-link' to='/#contact'>
								Contact
							</Link>
						</li>
						{token && email === "admin@admin.com" && (
							<li className='nav-item'>
								<Link className='nav-link' to='/admin'>
									Admin Control
								</Link>
							</li>
						)}
						{token ? (
							<>
								{email !== "admin@admin.com" && (
									<li className='nav-item'>
										<Link
											data-testid='nav-mybookings'
											className='nav-link'
											to='/mybookings'>
											My Bookings
										</Link>
									</li>
								)}
								<li className='nav-item'>
									<p
										className='nav-link'
										role='button'
										data-testid='nav-logout'
										onClick={handleLogout}
										style={{ marginBottom: "0px" }}>
										Logout
									</p>
								</li>
							</>
						) : (
							<li className='nav-item'>
								<Link className='nav-link' to='/signin'>
									Login
								</Link>
							</li>
						)}
					</ul>
				</div>
			</div>
		</nav>
	);
}

export default Navbar;
