import "./About.css";

function About() {
	return (
		<div className='aboutMe container' id='about'>
			<div className='row'>
				<div className='col-12'>
					<h4 data-testid='main-heading' className='mainHeading'>
						About
					</h4>
					<h4 className='smallText'>
						HypeBus is India’s largest online bus ticketing platform that has
						transformed bus travel in the country by bringing ease and convenience to
						millions of Indians who travel using buses. Founded in 2006, HypeBus is part
						of India’s leading online travel company MakeMyTrip Limited (NASDAQ: MMYT).
						By providing widest choice, superior customer service, lowest prices and
						unmatched benefits, HypeBus has served over 18 million customers. HypeBus
						has a global presence with operations across Indonesia, Singapore, Malaysia,
						Colombia and Peru apart from India.
					</h4>
				</div>
			</div>
		</div>
	);
}
export default About;
