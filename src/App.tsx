import Navbar from "./components/Navbar/Navbar";
import LandingPage from "./pages/LandingPage/LandingPage";
import ErrorPage from "./pages/ErrorPage/ErrorPage";
import Footer from "./components/Footer/Footer";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import SignInPage from "./pages/SigninPage/SignInPage";
import SignUpPage from "./pages/SignupPage/SignUpPage";
import "./App.css";
import BusList from "./pages/BusList/BusList";
import MyBookings from "./pages/MyBookings/MyBookings";
import Admin from "./pages/Admin/Admin";

function App() {
	return (
		<div className='App'>
			<Router>
				<Navbar />
				<Routes>
					<Route path='/' element={<LandingPage />} />
					<Route path='/signin' element={<SignInPage />} />
					<Route path='/signup' element={<SignUpPage />} />
					<Route path='/buses' element={<BusList />} />
					<Route path='/mybookings' element={<MyBookings />} />
					<Route path='/admin' element={<Admin />} />
					<Route path='/*' element={<ErrorPage />} />
				</Routes>
				<Footer />
			</Router>
			<ToastContainer />
		</div>
	);
}

export default App;
