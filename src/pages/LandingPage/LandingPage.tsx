import Intro from "../../components/Intro/Intro";
import About from "../../components/About/About";
import Contact from "../../components/Contact/Contact";

function LandingPage() {
	return (
		<div className='landing-page'>
			<Intro />
			<About />
			<Contact />
		</div>
	);
}

export default LandingPage;
