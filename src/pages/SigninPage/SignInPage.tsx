import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { errorHandler } from "../../utils/errorHandler";
import { loginUser } from "../../api/login";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Notify from "../../components/Notify";
import { userSelector, setEmail, setPassword, setToken } from "../../features/users/UserSlice";
import { useSelector, useDispatch } from "react-redux";

const theme = createTheme();

function SignInPage(props: any) {
	const dispatch = useDispatch();
	const { email, password, token } = useSelector(userSelector);
	let navigate = useNavigate();

	const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		const data = new FormData(event.currentTarget);
		console.log({
			email: data.get("email"),
			password: data.get("password"),
		});
	};
	const handleLogin = async () => {
		try {
			const result = await loginUser(email, password);
			dispatch(setToken(result.data.token)); // storing jwt-token in local storage for authorized API calls
			navigate("/buses");
		} catch (error) {
			errorHandler(error);
		}
	};
	useEffect(() => {
		if (token) {
			Notify(`Already Logged In`, "success");
			navigate("/buses");
		}
	}, []);

	return (
		<ThemeProvider theme={theme}>
			<Container component='main' maxWidth='xs' className='signin'>
				<CssBaseline />
				<Box
					sx={{
						// marginTop: 14,
						display: "flex",
						flexDirection: "column",
						alignItems: "center",
					}}>
					<Avatar sx={{ m: 1, marginTop: 14, bgcolor: "secondary.main" }}>
						<LockOutlinedIcon />
					</Avatar>
					<Typography component='h1' variant='h5'>
						Sign in
					</Typography>
					<Box component='form' onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
						<TextField
							margin='normal'
							required
							fullWidth
							id='email'
							label='Email Address'
							name='email'
							autoComplete='email'
							autoFocus
							value={email}
							onChange={(event) => {
								dispatch(setEmail(event.target.value));
							}}
						/>
						<TextField
							margin='normal'
							required
							fullWidth
							name='password'
							label='Password'
							type='password'
							id='password'
							autoComplete='current-password'
							value={password}
							onChange={(event) => {
								dispatch(setPassword(event.target.value));
							}}
						/>
						<FormControlLabel
							control={<Checkbox value='remember' color='primary' />}
							label='Remember me'
						/>
						<Button
							type='submit'
							fullWidth
							variant='contained'
							sx={{ mt: 3, mb: 2 }}
							onClick={handleLogin}>
							Sign In
						</Button>
						<Grid container>
							<Grid item xs>
								<Link href='#' variant='body2'>
									Forgot password?
								</Link>
							</Grid>
							<Grid item>
								<Link href='/signup' variant='body2'>
									{"Don't have an account? Sign Up"}
								</Link>
							</Grid>
						</Grid>
					</Box>
				</Box>
			</Container>
		</ThemeProvider>
	);
}
export default SignInPage;
