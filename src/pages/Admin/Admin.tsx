import dayjs from "dayjs";
import { useEffect } from "react";
import { getAllBuses } from "../../api/getAllBuses";
import { errorHandler } from "../../utils/errorHandler";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { resetBus } from "../../api/resetBus";
import Notify from "../../components/Notify";
import _ from "lodash";
import { busesSelector, setBuses } from "../../features/buses/BusSlice";
import { useSelector, useDispatch } from "react-redux";
import { userSelector } from "../../features/users/UserSlice";

function Bus(props: any) {
	const dispatch = useDispatch();
	const { token } = useSelector(userSelector);

	const { buses } = useSelector(busesSelector);
	const departure = dayjs(props.bus.departure);
	const arrival = dayjs(props.bus.arrival);

	let minutes = arrival.diff(departure, "minute");
	const hours = Math.floor(minutes / 60);
	minutes = minutes - hours * 60;

	const handleReset = async (busId: string) => {
		try {
			await resetBus(token, busId);
			await props.getAllBuses();
			const busesCopy = _.cloneDeep(buses);
			(busesCopy[props.id] as any).bookedSeats = [];
			dispatch(setBuses(busesCopy));
			Notify("Reset done", "success");
		} catch (error) {
			console.log(error);
			errorHandler(error);
		}
	};

	return (
		<div className='bus row' key={props.id}>
			<div className='col-lg-4' style={{ textAlign: "left" }}>
				<h4 style={{ fontSize: "32px" }}> {props.bus.name}</h4>
				<p>Bus ID - {props.bus._id}</p>
			</div>

			<div className='bus-details col-lg-6 mt-4'>
				<div className='d-flex'>
					<div>
						<p className='red-box'>{props.bus.from}</p>
						<b>{departure.format("DD MMM, HH:mmA")}</b>
					</div>
					<div>
						<ArrowForwardIcon style={{ margin: "5px 20px 22px 20px" }} />
						<p>{`(${hours}hr ${minutes}min)`}</p>
					</div>
					<div>
						<p className='red-box'>{props.bus.to}</p>
						<b>{arrival.format("DD MMM, HH:mmA")}</b>
					</div>
				</div>
			</div>
			<div className='col-lg-2'>
				<h4
					className='ms-auto'
					style={{
						width: "100%",
						fontSize: "32px",
						marginBottom: "10px",
					}}>
					<span style={{ fontSize: "24px", marginRight: "10px" }}>&#8377;</span>
					{props.bus.fare}
				</h4>
				<div className='book-button ms-auto'>
					<p style={{ textAlign: "center" }}>
						{" "}
						{props.bus.seats - props.bus.bookedSeats.length} seats Left
					</p>
					<button
						className='red-button'
						onClick={() => {
							handleReset(props.bus._id);
						}}>
						{" "}
						Reset{" "}
					</button>
					<button className='red-button mt-4'> Delete </button>
				</div>
			</div>
		</div>
	);
}

function Admin(props: any) {
	const dispatch = useDispatch();
	const { buses } = useSelector(busesSelector);

	const getBuses = async () => {
		try {
			const busList = await getAllBuses();
			dispatch(setBuses((busList.data as any).buses));
		} catch (error) {
			errorHandler(error);
			console.log(error);
		}
	};
	useEffect(() => {
		getBuses();
	}, []);
	return (
		<div className='admin'>
			<h4 className='mainHeading' style={{ marginTop: "100px" }}>
				Admin Controls
			</h4>
			<div className='bus-list container' style={{ paddingBottom: "100px" }}>
				<div className='d-flex justify-content-center'>
					<button className='red-button mx-4'> Add a Bus </button>
				</div>

				{buses.map((bus, id) => (
					<Bus key={id} id={id} bus={bus} getAllBuses={getAllBuses} />
				))}
			</div>
		</div>
	);
}
export default Admin;
