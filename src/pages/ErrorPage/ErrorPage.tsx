import "./ErrorPage.css";

function ErrorPage() {
	return (
		<div className='error'>
			<h1 style={{ fontSize: "320px" }}>404</h1>
			<p>This page could not be found.</p>
		</div>
	);
}
export default ErrorPage;
