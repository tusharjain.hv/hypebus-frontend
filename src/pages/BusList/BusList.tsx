import "./BusList.css";
import { useEffect, useState } from "react";
import { BusType } from "../../utils/types";
import { getAllBuses } from "../../api/getAllBuses";
import { errorHandler } from "../../utils/errorHandler";
import SelectSeatDrawer from "../../components/Drawers/SelectSeatDrawer";
import PassengerDetailDrawer from "../../components/Drawers/PassengerDetailDrawer";
import Bus from "../../components/Bus/Bus";
import Filter from "../../components/Filter/Filter";
import { busesSelector, setBuses } from "../../features/buses/BusSlice";
import { useSelector, useDispatch } from "react-redux";

function BusList() {
	const dispatch = useDispatch();
	const { buses } = useSelector(busesSelector);

	const [drawerIsOpen, setDrawerIsOpen] = useState(false);
	const [passengerDrawerIsOpen, setPassengerDrawerIsOpen] = useState(false);
	const [filter, setFilter] = useState({
		from: "",
		to: "",
		departure: null,
	});

	const [options, setOptions] = useState<string[]>([]);

	const toggleDrawer = (open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
		if (
			event.type === "keydown" &&
			((event as React.KeyboardEvent).key === "Tab" ||
				(event as React.KeyboardEvent).key === "Shift")
		) {
			return;
		}

		setDrawerIsOpen(open);
	};
	const togglePassengerDrawer =
		(open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
			if (
				event.type === "keydown" &&
				((event as React.KeyboardEvent).key === "Tab" ||
					(event as React.KeyboardEvent).key === "Shift")
			) {
				return;
			}

			setPassengerDrawerIsOpen(open);
		};
	useEffect(() => {
		const getBuses = async () => {
			try {
				const busList = await getAllBuses();
				dispatch(setBuses((busList.data as any).buses));

				let possibleOptions: string[] = [];
				(busList.data as any).buses.forEach((bus: BusType) => {
					if (!possibleOptions.includes(bus.from)) possibleOptions.push(bus.from);
					if (!possibleOptions.includes(bus.to)) possibleOptions.push(bus.to);
				});
				setOptions(possibleOptions);
				console.log(buses, busList);
			} catch (error) {
				errorHandler(error);
				console.log(error);
			}
		};
		getBuses();
	}, []);

	return (
		<>
			<Filter filter={filter} setFilter={setFilter} options={options} />
			<div className='bus-list container' style={{ paddingBottom: "100px" }}>
				{buses.map((bus, id) => (
					<Bus key={id} id={id} bus={bus} setDrawerIsOpen={setDrawerIsOpen} />
				))}
			</div>
			<SelectSeatDrawer
				drawerIsOpen={drawerIsOpen}
				setDrawerIsOpen={setDrawerIsOpen}
				setPassengerDrawerIsOpen={setPassengerDrawerIsOpen}
				toggleDrawer={toggleDrawer}
			/>
			<PassengerDetailDrawer
				setDrawerIsOpen={setDrawerIsOpen}
				setPassengerDrawerIsOpen={setPassengerDrawerIsOpen}
				passengerDrawerIsOpen={passengerDrawerIsOpen}
				togglePassengerDrawer={togglePassengerDrawer}
			/>
		</>
	);
}
export default BusList;
