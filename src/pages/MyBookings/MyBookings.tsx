import { useEffect } from "react";
import Booking from "../../components/Booking/Booking";
import { getMyBookings } from "../../api/getMyBookings";
import { setBookings } from "../../features/bookings/BookingSlice";
import { useDispatch, useSelector } from "react-redux";
import { userSelector } from "../../features/users/UserSlice";

function MyBookings() {
	const dispatch = useDispatch();
	const { token } = useSelector(userSelector);

	useEffect(() => {
		const getBookings = async () => {
			const result = await getMyBookings(token);
			dispatch(setBookings(result.data.bookings));
		};
		getBookings();
	}, []);
	return (
		<div className='mybookings'>
			<h4 className='mainHeading' style={{ marginTop: "100px" }}>
				My Bookings
			</h4>
			<Booking />
		</div>
	);
}
export default MyBookings;
