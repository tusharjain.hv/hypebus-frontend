import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useNavigate } from "react-router-dom";
import Notify from "../../components/Notify";
import { useEffect } from "react";
import _ from "lodash";
import { registerUser } from "../../api/register";
import { errorHandler } from "../../utils/errorHandler";
import { userSelector, setNewUser } from "../../features/users/UserSlice";
import { useSelector, useDispatch } from "react-redux";

const theme = createTheme();

function SignUpPage() {
	const dispatch = useDispatch();
	const { newUser, token } = useSelector(userSelector);
	let navigate = useNavigate();

	const handleChange = (event: { target: { name: string; value: string } }) => {
		const { name, value } = event.target;
		let newUserCopy: any = _.cloneDeep(newUser);
		newUserCopy[name] = value;
		dispatch(setNewUser(newUserCopy));
	};
	const handleRegister = async () => {
		try {
			await registerUser(
				`${newUser.fname} ${newUser.lname}`,
				newUser.email,
				newUser.password
			);
			Notify("User registered successfully, Please Login!", "success");
			navigate("/signin");
		} catch (error) {
			errorHandler(error);
		}
	};

	useEffect(() => {
		if (token) {
			Notify(`Already Logged In`, "info");
			navigate("/buses");
		}
	}, []);
	return (
		<ThemeProvider theme={theme}>
			<Container component='main' maxWidth='xs' className='signup'>
				<CssBaseline />
				<Box
					sx={{
						marginTop: 14,
						display: "flex",
						flexDirection: "column",
						alignItems: "center",
					}}>
					<Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
						<LockOutlinedIcon />
					</Avatar>
					<Typography component='h1' variant='h5'>
						Sign up
					</Typography>
					<Box component='form' noValidate sx={{ mt: 3 }}>
						<Grid container spacing={2}>
							<Grid item xs={12} sm={6}>
								<TextField
									autoComplete='given-name'
									name='fname'
									value={newUser.fname}
									onChange={handleChange}
									required
									fullWidth
									id='firstName'
									label='First Name'
									autoFocus
								/>
							</Grid>
							<Grid item xs={12} sm={6}>
								<TextField
									required
									fullWidth
									id='lName'
									label='Last Name'
									name='lname'
									value={newUser.lname}
									onChange={handleChange}
									autoComplete='family-name'
								/>
							</Grid>
							<Grid item xs={12}>
								<TextField
									required
									fullWidth
									id='email'
									label='Email Address'
									name='email'
									value={newUser.email}
									onChange={handleChange}
									autoComplete='email'
								/>
							</Grid>
							<Grid item xs={12}>
								<TextField
									required
									fullWidth
									name='password'
									value={newUser.password}
									onChange={handleChange}
									label='Password'
									type='password'
									id='password'
									autoComplete='new-password'
								/>
							</Grid>
							<Grid item xs={12}>
								<FormControlLabel
									control={<Checkbox value='allowExtraEmails' color='primary' />}
									label='I want to receive inspiration, marketing promotions and updates via email.'
								/>
							</Grid>
						</Grid>
						<Button
							onClick={handleRegister}
							fullWidth
							variant='contained'
							sx={{ mt: 3, mb: 2 }}>
							Sign Up
						</Button>
						<Grid container justifyContent='flex-end'>
							<Grid item>
								<Link href='/signin' variant='body2'>
									Already have an account? Sign in
								</Link>
							</Grid>
						</Grid>
					</Box>
				</Box>
			</Container>
		</ThemeProvider>
	);
}

export default SignUpPage;
