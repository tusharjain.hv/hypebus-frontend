import React, { PropsWithChildren } from "react";
import { render } from "@testing-library/react";
import type { RenderOptions } from "@testing-library/react";
import { configureStore } from "@reduxjs/toolkit";
import type { PreloadedState } from "@reduxjs/toolkit";
import { Provider } from "react-redux";
import busesSliceReducer from "../features/buses/BusSlice";
import bookingsSliceReducer from "../features/bookings/BookingSlice";
import userSliceReducer from "../features/users/UserSlice";

export function renderWithProviders(
	ui: React.ReactElement,
	{
		preloadedState = {},
		// Automatically create a store instance if no store was passed in
		store = configureStore({
			reducer: {
				busesStore: busesSliceReducer,
				bookingsStore: bookingsSliceReducer,
				userStore: userSliceReducer,
			},
			preloadedState,
		}),
		...renderOptions
	}: any = {}
) {
	function Wrapper({ children }: PropsWithChildren<{}>): JSX.Element {
		// console.log("checking", store);
		return <Provider store={store}>{children}</Provider>;
	}

	// Return an object with the store and all of RTL's query functions
	return { store, ...render(ui, { wrapper: Wrapper, ...renderOptions }) };
}
