export interface UserType {
	name: string;
	email: string;
	password: string;
}
export interface BusType {
    _id:string,
    busId:string
    name: string
	seats: number
	size: string
	bookedSeats: number[],
	from: string
	to: string
	departure: Date
	arrival: Date
	fare: number
}

export interface BasicResponse{
    status: number,
    message: string,
	data: {}
}
export interface Passenger{
    name:string,
    gender:string,
    age:number
}

export interface BookingType{
    userId: string
    busId: string
    phone: string
    amount:number
    seats:number[],
	passengers:Passenger[]
}