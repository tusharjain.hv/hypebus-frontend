import Notify from "../components/Notify";

export const errorHandler = (error: any) => {
    console.log(error)
	//Server responded but with a error response
	if (error.response) Notify(`${error.response.data.message} 😥`, "error");
	//Server didn't responded but a request was made
	else if (error.request)
		Notify(
			`Server didn't responded!😥 Please check your network or try after sometime `,
			"error"
		);
	else if (error.message==="Please Login!")
		Notify(
			error.message,
			"error"
		);
	//All other types of error if arises
	else Notify(`Something went wrong ! ⚠`, "error");
};
