import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {  BookingType, Passenger } from "../../utils/types";

export interface BookingState {
    bookings: object[];
    newBooking: BookingType;
	loading: boolean;
	errors: unknown;
}

const initialState: BookingState = {
    bookings: [],
    newBooking:{
		passengers: [] as Passenger[],
		seats: [],
		busId: "",
        phone: "",
        userId: "",
        amount:0
	},
	loading: false,
	errors: "",
};

const bookingSlice = createSlice({
	name: "bookings",
	initialState,
	reducers: {
		setLoading: (state, { payload }: PayloadAction<boolean>) => {
			state.loading = payload;
		},

		setErrors: (state, { payload }: PayloadAction<unknown>) => {
			state.errors = payload;
		},

		setBookings: (state, { payload }: PayloadAction<object[]>) => {
			state.bookings = payload;
        },
        setNewBooking: (state, { payload }: PayloadAction<BookingType>) => {
			state.newBooking = payload;
        },
	},
});

export const { setLoading, setErrors, setBookings,setNewBooking } = bookingSlice.actions;

export default bookingSlice.reducer;

export const bookingsSelector = (state: { bookingsStore: BookingState }) => state.bookingsStore;
