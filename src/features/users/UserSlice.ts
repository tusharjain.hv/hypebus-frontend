import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface UserState {
    email: string,
    password:string,
    name: string,
    token:string | null,
    newUser: {
        fname: string,
        lname: string,
        email: string,
        password:string
    }
	loading: boolean;
	errors: unknown;
}

const initialState: UserState = {
    email: "",
    password: "",
    name: "",
    token:null,
    newUser: {
        fname: "",
        lname: "",
        email: "",
        password:""
    },
	loading: false,
	errors: "",
};

const userSlice = createSlice({
	name: "user",
	initialState,
	reducers: {
		setLoading: (state, { payload }: PayloadAction<boolean>) => {
			state.loading = payload;
		},

		setErrors: (state, { payload }: PayloadAction<unknown>) => {
			state.errors = payload;
		},

		setEmail: (state, { payload }: PayloadAction<string>) => {
			state.email = payload;
        },
        setPassword: (state, { payload }: PayloadAction<string>) => {
			state.password = payload;
        },
        setName: (state, { payload }: PayloadAction<string>) => {
			state.name = payload;
        },
        setToken: (state, { payload }: PayloadAction<string | null>) => {
			state.token = payload;
        },
        setNewUser: (state, { payload }: PayloadAction<{fname:string,lname:string,email:string,password:string}>) => {
			state.newUser = payload;
        },
        
	},
});

export const { setLoading, setErrors, setEmail,setPassword,setName,setNewUser,setToken } = userSlice.actions;

export default userSlice.reducer;

export const userSelector = (state: { userStore: UserState }) => state.userStore;
