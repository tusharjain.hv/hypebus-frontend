import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { BusType } from "../../utils/types";

export interface BusState {
    buses: object[];
    selectedBus: BusType,
    selectedSeats:number[],
	loading: boolean;
	errors: unknown;
}

const initialState: BusState = {
    buses: [],
    selectedBus:{
		_id: "",
		busId: "",
		name: "",
		seats: 0,
		size: "",
		bookedSeats: [],
		from: "",
		to: "",
		departure: new Date(),
		arrival: new Date(),
		fare: 0,
    },
    selectedSeats:[],
	loading: false,
	errors: "",
};

const busSlice = createSlice({
	name: "buses",
	initialState,
	reducers: {
		setLoading: (state, { payload }: PayloadAction<boolean>) => {
			state.loading = payload;
		},

		setErrors: (state, { payload }: PayloadAction<unknown>) => {
			state.errors = payload;
		},

		setBuses: (state, { payload }: PayloadAction<object[]>) => {
			state.buses = payload;
        },
        setSelectedBus: (state, { payload }: PayloadAction<BusType>) => {
			state.selectedBus = payload;
        },
        setSelectedSeats: (state, { payload }: PayloadAction<number[]>) => {
			state.selectedSeats = payload;
        },
        
	},
});

export const { setLoading, setErrors, setBuses,setSelectedBus,setSelectedSeats } = busSlice.actions;

export default busSlice.reducer;

export const busesSelector = (state: { busesStore: BusState }) => state.busesStore;
