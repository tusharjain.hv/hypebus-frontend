import axios from "axios";
const apiBaseURL = process.env.REACT_APP_HYPERBUS_API || `http://localhost:5000`;

export const logoutUser = async (token:string|null) => {
    try {
        if(!token) throw new Error("Please Login!")
		const options = {
			method: "POST",
            url: `${apiBaseURL}/user/logout`,
			headers: {
                "content-type": "application/json",
                "Authorization":`Bearer ${token}`
			}
            
        };
        const result = await axios(options);
		return result.data;
	} catch (error) {
		throw error;
	}
};
