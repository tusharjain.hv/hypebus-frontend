import axios from "axios";
const apiBaseURL = process.env.REACT_APP_HYPERBUS_API || `http://localhost:5000`;

export const searchBuses = async (filter: { from: string; to: string; departure: string }) => {
	try {
		const options = {
			method: "GET",
            url: `${apiBaseURL}/bus/search`,
            params: filter,
			headers: {
				"content-type": "application/json",
			},
            
        };
        console.log(options)

		const result = await axios(options);
		return result.data;
	} catch (error) {
		throw error;
	}
};
