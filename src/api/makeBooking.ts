import axios from "axios";
import { BookingType } from "../utils/types";
const apiBaseURL = process.env.REACT_APP_HYPERBUS_API || `http://localhost:5000`;

export const makeBooking = async (token:string|null,booking:BookingType) => {
    try {
        if (!token) {
            throw new Error("Please Login!")
        }
		const options = {
			method: "POST",
            url: `${apiBaseURL}/booking/add`,
			headers: {
                "content-type": "application/json",
                "Authorization":`Bearer ${token}`
            },
            data:booking
            
        };
        const result = await axios(options);
		return result.data;
	} catch (error) {
		throw error;
	}
};
