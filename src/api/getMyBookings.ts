import axios from "axios";
const apiBaseURL = process.env.REACT_APP_HYPERBUS_API || `http://localhost:5000`;

export const getMyBookings = async (token:string | null) => {
    try {
        console.log('inside',token)
        if (!token) {
            throw new Error("Please Login!")
        }
		const options = {
			method: "GET",
            url: `${apiBaseURL}/booking/show`,
			headers: {
                "content-type": "application/json",
                "Authorization":`Bearer ${token}`
            },
            
        };
        console.log('inside getMybOOkings()',options)
        const result = await axios(options);
		return result.data;
	} catch (error) {
		throw error;
	}
};
