import axios from "axios";

const apiBaseURL = process.env.REACT_APP_HYPERBUS_API || `http://localhost:5000`;

export const loginUser = async (email:string,password:string) => {
	try {
		const options = {
			method: "POST",
            url: `${apiBaseURL}/user/login`,
            data: {email,password},
			headers: {
				"content-type": "application/json",
			},
            
        };
        console.log(options)
        const result = await axios(options);
		return result.data;
	} catch (error) {
		throw error;
	}
};
