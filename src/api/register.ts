import axios from "axios";
const apiBaseURL = process.env.REACT_APP_HYPERBUS_API || `http://localhost:5000`;

export const registerUser = async (name:string,email:string,password:string) => {
    try {
		const options = {
			method: "POST",
            url: `${apiBaseURL}/user/add`,
			headers: {
                "content-type": "application/json",
            },
            data:{name,email,password}
            
        };
        const result = await axios(options);
		return result.data;
	} catch (error) {
		throw error;
	}
};
