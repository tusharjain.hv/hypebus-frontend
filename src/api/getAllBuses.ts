import axios from "axios";

const apiBaseURL = process.env.REACT_APP_HYPERBUS_API || `http://localhost:5000`;

export const getAllBuses = async () => {
    try {
        console.log(process.env,process.env.REACT_APP_HYPERBUS_API)
        const result = await axios.get(`${apiBaseURL}/bus/all`);
        return result.data;
    }
    catch (error) {
        throw error;
    };
};