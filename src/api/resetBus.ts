import axios from "axios";
const apiBaseURL = process.env.REACT_APP_HYPERBUS_API || `http://localhost:5000`;

export const resetBus = async (token:string|null,busId:string) => {
    try {
        if(!token) throw new Error("Please Login!")
		const options = {
			method: "POST",
            url: `${apiBaseURL}/bus/reset`,
            data: {busId},
			headers: {
                "content-type": "application/json",
                "Authorization":`Bearer ${token}`
			},
            
        };
        console.log(options)
        const result = await axios(options);
		return result.data;
	} catch (error) {
		throw error;
	}
};
