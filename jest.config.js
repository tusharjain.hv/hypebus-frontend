module.exports = {
	preset: "ts-jest",
	testEnvironment: "node",
	forceExit: true,
	verbose: true,
	transform: {
		"^.+\\.(ts|tsx)$": "ts-jest",
	},
	transformIgnorePatterns: ["/node_modules/(?!(axios)/)"],
	moduleNameMapper: {
		"\\.(css|less|sass|scss)$": "<rootDir>/__mocks__/styleMock.ts",
	},
};
