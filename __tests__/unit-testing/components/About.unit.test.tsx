import { act, render, screen } from "@testing-library/react";
import About from "../../../src/components/About/About";

describe("Testing About", () => {
	test("it should render about section", async () => {
		// Render
		await act(async () => {
			render(<About />);
		});

		// Act
		const heading = screen.getByTestId("main-heading");

		// Assertion
		expect(heading.textContent).toBe("About");
	});
});
