import { act, render, screen } from "@testing-library/react";
import Contact from "../../../src/components/Contact/Contact";

describe("Testing Contact", () => {
	test("it should render contact info section", async () => {
		// Render
		await act(async () => {
			render(<Contact />);
		});

		// Act
		const heading = screen.getByTestId("main-heading");

		// Assertion
		expect(heading.textContent).toBe("Contact Info");
	});
});
