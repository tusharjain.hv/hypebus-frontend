import { act, fireEvent, queryByTestId, render, screen } from "@testing-library/react";
import { createMemoryHistory, MemoryHistory } from "history";
import Navbar from "../../../src/components/Navbar/Navbar";
import { Router } from "react-router";
import { getUser } from "../../../src/api/getUser";
import { logoutUser } from "../../../src/api/logout";
import { renderWithProviders } from "../../../src/utils/util-for-unit-tests";

jest.mock("../../../src/api/getUser");
jest.mock("../../../src/api/logout");

const mockGetUser = getUser as jest.Mock;
const mockLogout = logoutUser as jest.Mock;

describe("Testing Navbar navigations", () => {
	mockGetUser.mockResolvedValue({
		name: "Tushar",
		email: "tushar@gmail.com",
	});
	mockLogout.mockResolvedValue({});

	test("it should redirect to buses section /buses", async () => {
		// Render
		const history = createMemoryHistory();
		await act(async () => {
			renderWithProviders(
				<Router location={"/"} navigator={history}>
					<Navbar />
				</Router>
			);
		});

		// Act
		await act(async () => {
			fireEvent.click(screen.getByTestId("nav-buses"));
		});

		// Assertion
		expect(history.location.pathname).toBe("/buses");
	});

	test("it should redirect to about section /#about", async () => {
		// Render
		const history = createMemoryHistory();
		await act(async () => {
			renderWithProviders(
				<Router location={"/"} navigator={history}>
					<Navbar />
				</Router>
			);
		});

		// Act
		await act(async () => {
			fireEvent.click(screen.getByTestId("nav-about"));
		});

		// Assertion
		expect(history.location.hash).toBe("#about");
	});

	test("it should redirect to home page - /", async () => {
		// Render
		const history = createMemoryHistory();
		await act(async () => {
			renderWithProviders(
				<Router location={"/"} navigator={history}>
					<Navbar />
				</Router>
			);
		});

		// Act
		await act(async () => {
			fireEvent.click(screen.getByTestId("nav-brand"));
		});

		// Assertion
		expect(history.location.pathname).toBe("/");
	});

	test("it should redirect to My Bookings - /mybookings", async () => {
		// Render
		const history = createMemoryHistory();
		await act(async () => {
			renderWithProviders(
				<Router location={"/"} navigator={history}>
					<Navbar />
				</Router>,
				{ preloadedState: { userStore: { token: "token" } } }
			);
		});

		// Act
		await act(async () => {
			fireEvent.click(screen.getByTestId("nav-mybookings"));
		});

		// Assertion
		expect(history.location.pathname).toBe("/mybookings");
	});
});

describe("Testing Logout Button", () => {
	mockGetUser.mockResolvedValue({
		name: "Tushar",
		email: "tushar@gmail.com",
	});
	mockLogout.mockResolvedValue({});
	let container: HTMLElement;

	test("it should redirect to buses section /buses", async () => {
		// Render
		const history = createMemoryHistory();
		await act(async () => {
			const options = renderWithProviders(
				<Router location={"/"} navigator={history}>
					<Navbar />
				</Router>,
				{ preloadedState: { userStore: { token: "token" } } }
			);
			container = options.container;
		});

		// Act
		const logoutButton = screen.getByTestId("nav-logout");

		await act(async () => {
			fireEvent.click(logoutButton);
		});

		// Assertion
		expect(queryByTestId(container, "nav-logout")).not.toBeTruthy();
	});
});
