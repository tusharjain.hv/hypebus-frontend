import { act, fireEvent, queryByTestId, render, screen } from "@testing-library/react";
import Booking from ".../../../src/components/Booking/Booking";
import { renderWithProviders } from "../../../src/utils/util-for-unit-tests";

const mockBookings = [
	{
		_id: "12345",
		name: "Satyam Travels",
		from: "Delhi",
		departure: new Date(),
		to: "Varanasi",
		arrival: new Date(),
		seats: [1, 2],
		amount: 1000,
		passengers: [
			{ name: "User1", gender: "male", age: 20 },
			{ name: "User1", gender: "male", age: 20 },
		],
	},
];

describe("Testing Booking", () => {
	test("it should render my bookings section", async () => {
		// Render
		await act(async () => {
			renderWithProviders(<Booking />, {
				preloadedState: { bookingsStore: { bookings: mockBookings } },
			});
		});

		// Act
		const busId = screen.getByTestId("bus-id-0");

		// Assertion
		expect(busId.textContent).toBe(mockBookings[0]._id);
	});
});

describe("Testing Collapsible Passenger Details section", () => {
	test("it should not display passenger details menu", async () => {
		// Render
		await act(async () => {
			const { container } = renderWithProviders(<Booking />, {
				preloadedState: { bookingsStore: { bookings: mockBookings } },
			});
			expect(queryByTestId(container, "collapse-menu-0")).not.toBeTruthy();
		});
	});
});

describe("Testing Collapsible Passenger Details section", () => {
	test("it should display passenger details menu", async () => {
		// Render
		await act(async () => {
			renderWithProviders(<Booking />, {
				preloadedState: { bookingsStore: { bookings: mockBookings } },
			});
		});

		// Act
		const collapseButton = screen.getByTestId("collapse-button-0");
		await act(async () => {
			fireEvent.click(collapseButton);
		});

		// Assertion
		expect(screen.getByTestId("collapse-menu-0")).toBeTruthy();
	});
});
