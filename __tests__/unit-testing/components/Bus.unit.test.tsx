import { act, fireEvent, queryByTestId, render, screen } from "@testing-library/react";
import Bus from "../../../src/components/Bus/Bus";
import { renderWithProviders } from "../../../src/utils/util-for-unit-tests";

const mockBus = {
	_id: "12345",
	name: "Satyam Travels",
	from: "Delhi",
	departure: new Date(),
	to: "Varanasi",
	type: "3x2",
	arrival: new Date(),
	seats: 24,
	bookedSeats: [1, 2],
	fare: 1000,
};
describe("Testing Bus", () => {
	test("it should render buses section", async () => {
		// Render
		await act(async () => {
			renderWithProviders(<Bus id={0} bus={mockBus} setDrawerIsOpen={() => {}} />);
		});

		// Act
		const busName = screen.getByTestId("bus-name-0");

		// Assertion
		expect(busName.textContent).toBe(mockBus.name);
	});
});
