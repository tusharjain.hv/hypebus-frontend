import { act, fireEvent, queryByTestId, render, screen } from "@testing-library/react";
import BusList from "../../../src/pages/BusList/BusList";
import { getAllBuses } from "../../../src/api/getAllBuses";
import { createMemoryHistory, MemoryHistory } from "history";
import { renderWithProviders } from "../../../src/utils/util-for-unit-tests";
import { Router } from "react-router-dom";
jest.mock("../../../src/api/getAllBuses");

const mockGetAllBuses = getAllBuses as jest.Mock;

const mockBuses = [
	{
		_id: "12345",
		name: "Satyam Travels",
		from: "Delhi",
		departure: new Date(),
		to: "Varanasi",
		type: "3x2",
		arrival: new Date(),
		seats: 24,
		bookedSeats: [1, 2],
		fare: 1000,
	},
];

describe("Testing Bus List Section", () => {
	mockGetAllBuses.mockResolvedValue({ data: { buses: mockBuses } });
	let container: HTMLElement;
	test("it should render buses section", async () => {
		// Render
		const history = createMemoryHistory();
		await act(async () => {
			const options = renderWithProviders(
				<Router location={"/"} navigator={history}>
					<BusList />
				</Router>,
				{
					preloadedState: {
						busesStore: {
							buses: mockBuses,
							selectedSeats: [],
							selectedBus: mockBuses[0],
						},
						userStore: {
							token: "mockToken",
						},
					},
				}
			);
			container = options.container;
		});

		// Act
		const goButton = screen.getByTestId("go-button-0");
		await act(async () => {
			fireEvent.click(goButton);
		});
		const heading = screen.getByTestId("seat-map-heading");

		// Assertion
		expect(heading.innerHTML).toBe("Select Your Seat");
	});
});
