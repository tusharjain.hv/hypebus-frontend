import { Router } from "react-router-dom";
import renderer from "react-test-renderer";
import { createMemoryHistory } from "history";
import LandingPage from "../../src/pages/LandingPage/LandingPage";

test("Testing Snapshot of Landing Page", async () => {
	const history = createMemoryHistory();
	const tree = renderer
		.create(
			<Router location={"/"} navigator={history}>
				<LandingPage />
			</Router>
		)
		.toJSON();
	expect(tree).toMatchSnapshot();
});
