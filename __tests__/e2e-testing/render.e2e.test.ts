import puppeteer from "puppeteer";
jest.setTimeout(60000);

describe("Basic  e2e tests", () => {
  it("Should render the page", async () => {
    const browser = await puppeteer.launch();
		const page = await browser.newPage();
		// Set a definite size for the page viewport so view is consistent across browsers
		await page.setViewport({
			width: 1366,
			height: 768,
			deviceScaleFactor: 1,
		});

    await page.goto("https://main.d3fw9dhx7uh24t.amplifyapp.com");
    const title = await page.title();
    expect(title).toBe('React App');
    await browser.close();
	});
});
