import puppeteer, { Browser, Page } from "puppeteer";
jest.setTimeout(60000);

let browser: Browser;
let page: Page;

describe("Basic  e2e tests", () => {
	beforeAll(async () => {
		browser = await puppeteer.launch();
		page = await browser.newPage();
		// Set a definite size for the page viewport so view is consistent across browsers
		await page.setViewport({
			width: 1366,
			height: 768,
			deviceScaleFactor: 1,
		});
		await page.goto("https://main.d3fw9dhx7uh24t.amplifyapp.com");
	});

	it("Should render the heading", async () => {
		const h1Handle = await page.$(".landingPage h1");
		const html = await page.evaluate(
			(h1Handle) => (h1Handle ? h1Handle.innerHTML : null),
			h1Handle
		);
		console.log(html);
		expect(html).toBe("HypeBus <br>");
	});

	it("Should redirect to /buses", async () => {
		const linkSelector = "a[data-testid='nav-buses']";
		// const busesNav = await page.$(linkSelector);
		// console.log('tushar',busesNav);
		// busesNav?.click()
		// const html = await page.evaluate((h1Handle) => h1Handle ? h1Handle.innerHTML : null, h1Handle);
		// console.log(html);
		// expect(html).toBe("HypeBus <br>");
		expect(true).toBe(true);
	});

	afterAll(async () => {
		await browser.close();
	});
});
